var años = [];

var meses = ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN",
    "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"];

var tipos = ["mensual", "bimestre", "trimestre", "cuatrimestre", "semestre"];

function crearForm() {
    var borrar = document.getElementById("imp");
    borrar.innerHTML = "";

    var tabla = document.getElementById("imp");
    var form1 = "<form>";
    form1 += "<h6 >Ingresos del año</h6>";
    form1 += "<div><input type='number' id='ano' placeholder='1999'></div>" + "<br>";

    for (i = 0; i < 12; i++) {
        form1 += "<label>" + meses[i] + "&nbsp;&nbsp;&nbsp;</label>";
        form1 += "<input type='number'  name='valor' value='" + ((i + 1) * 100) + "'/><br>";
    }
    form1 += "<button type='button' class='btn btn-default' onclick='guardar()'>Guardar</button>";
    form1 += "</form>";
    tabla.innerHTML = form1;
}

function guardar() {
    var borrar = document.getElementById("imp");
    let nombre_año = Number(document.getElementById("ano").value);
    let datos = document.getElementsByName("valor");
    let vector = [];

    datos.forEach(input => {
        vector.push(input.value);
    });

    var año = {
        nombre: nombre_año,
        saldos: vector
    }

    años.push(año);

    borrar.innerHTML = "";
}

function mostrar() {
    var borrar = document.getElementById("imp");
    borrar.innerHTML = "";
    var tabla = document.getElementById("imp");
    var form2 = "<form>";
    form2 += "<select id='s1'>";

    for (let index = 0; index < años.length; index++) {
        form2 += "<option>" + años[index]["nombre"] + "</option>";
    }
    form2 += "</select> <br> <br>";

    form2 += "<select id='s2'>";
    for (let i = 0; i < tipos.length; i++) {
        form2 += "<option>" + tipos[i] + "</option>";
    }
    form2 += "</select> <br> <br>";
    form2 += "<button type='button' class='btn btn-default' onclick='crearTable()'>mostrar</button>";
    form2 += "</form>";
    tabla.innerHTML = form2;
}

function crearTable() {
    var tabla = document.getElementById("imp2");
    let a = document.getElementById("s1").value;
    let b = document.getElementById("s2").value;
    let valor;
    let tipo;

    for (let index = 0; index < años.length; index++) {
        if (años[index]["nombre"] == a) {
            valor = años[index]["saldos"];
        };
    }

    for (let index = 0; index < tipos.length; index++) {
        if (tipos[index] == b) {
            tipo = index + 1;
        }
    }

    if (tipo == 5) {
        tipo++;
    }

    input = operar(valor, tipo);
    
    draw_chart(input["grupos"],input["saldos"]);

}

function operar(valores, pareja) {
    let salida = [];
    let costo = [];
    let cantidad = 0;

    cantidad = pareja == 1 ? 12 : pareja == 2 ? 6 : pareja == 3 ? 4 : pareja == 4 ? 3 : 2;

    let j = 0;
    let p = pareja;
    let suma = 0;

    for (let i = 0; i < cantidad; i++) {
        for (let k = j; k <= j + p - 1; k++) {
            suma += Number(valores[k]);
        }
        salida.push(meses[j] + "-" + meses[j + p - 1]);
        costo.push(suma);
        j += p;
        suma = 0;
    }

    var out = {
        grupos: salida,
        saldos: costo
    }
    return out;
}

function draw() {

    drawChart(57);
    google.charts.setOnLoadCallback(drawChart);

}

function draw_chart(datos, valores) {
    drawChart(datos, valores);
    drawTable(datos, valores);
    google.charts.setOnLoadCallback(drawChart);
    google.charts.setOnLoadCallback(drawTable);

}

